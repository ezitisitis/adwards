<?php

namespace Database\Factories\Jury;

use App\Models\Jury\JuryGroup;
use Illuminate\Database\Eloquent\Factories\Factory;

class JuryGroupFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = JuryGroup::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'year' => config('adwards.year'),
            'title' => $this->faker->text(),
            'order' => $this->faker->numberBetween(0, 65535),
        ];
    }
}
