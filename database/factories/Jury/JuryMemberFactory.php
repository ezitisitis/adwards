<?php

namespace Database\Factories\Jury;

use App\Models\Jury\JuryMember;
use Illuminate\Database\Eloquent\Factories\Factory;

class JuryMemberFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = JuryMember::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'image' => $this->faker->image('storage/app/public', 640, 640, null, false),
            'name' => $this->faker->name,
            'slug' => $this->faker->slug,
            'president' => $this->faker->boolean,
            'position' => $this->faker->jobTitle,
            'company' => $this->faker->company,
            'city' => $this->faker->city,
            'short_description' => $this->faker->realText(),
            'description' => '<div>' . $this->faker->realText() . '</div>',
        ];
    }
}
