<?php

namespace Database\Factories\Prize;

use App\Models\Prize;
use Illuminate\Database\Eloquent\Factories\Factory;

class PrizeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Prize\Prize::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->text(60),
            'description' => $this->faker->realText(),
            'image_path' => $this->faker->image('storage/app/public', 640, 640, null, false),
        ];
    }
}
