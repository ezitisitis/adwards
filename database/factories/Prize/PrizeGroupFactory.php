<?php

namespace Database\Factories\Prize;

use App\Models\Prize;
use Illuminate\Database\Eloquent\Factories\Factory;

class PrizeGroupFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Prize\PrizeGroup::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'year' => $this->faker->year,
            'title' => $this->faker->text(60),
        ];
    }
}
