<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJuryTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jury_groups', function (Blueprint $table) {
            $table->id();
            $table->year('year');
            $table->string('title');
            $table->unsignedSmallInteger('order')->default(0);
            $table->timestamps();
        });

        Schema::create('jury_members', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('jury_group_id')->nullable();
            $table->foreign('jury_group_id')
                ->references('id')
                ->on('jury_groups')
                ->onDelete('cascade');
            $table->string('image');
            $table->string('name');
            $table->string('slug');
            $table->boolean('president')->default(0);
            $table->string('position');
            $table->string('company');
            $table->string('city');
            $table->string('short_description');
            $table->text('description');
            $table->unsignedSmallInteger('order')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jury_members');
        Schema::dropIfExists('jury_groups');
    }
}
