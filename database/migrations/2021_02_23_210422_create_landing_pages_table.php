<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLandingPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('landing_pages', function (Blueprint $table) {
            $table->id();
            $table->string('hero')->nullable();
            $table->string('hero_text')->nullable();
            $table->string('hero_link')->nullable();
            $table->year('year');
            $table->string('heading_first_line')->nullable();
            $table->string('heading_second_line')->nullable();
            $table->text('top_description')->nullable();
            $table->text('detail_description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('landing_pages');
    }
}
