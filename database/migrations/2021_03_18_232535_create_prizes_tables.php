<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrizesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prize_groups', function (Blueprint $table) {
            $table->id();
            $table->year('year');
            $table->string('title');
            $table->unsignedSmallInteger('order')->default(0);
            $table->timestamps();
        });

        Schema::create('prizes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('prize_group_id');
            $table->foreign('prize_group_id')
                ->references('id')
                ->on('prize_groups')
                ->onDelete('cascade');
            $table->string('title');
            $table->text('description');
            $table->string('image_path');
            $table->unsignedSmallInteger('order')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prizes');
        Schema::dropIfExists('prize_groups');
    }
}
