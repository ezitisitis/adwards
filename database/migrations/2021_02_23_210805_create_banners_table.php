<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banner_groups', function (Blueprint $table) {
            $table->id();
            $table->year('year');
            $table->string('title')->nullable(true);
            $table->unsignedSmallInteger('order')->default(0);
            $table->timestamps();
        });

        Schema::create('banner_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('banner_group_id')->nullable();
            $table->foreign('banner_group_id')
                ->references('id')
                ->on('banner_groups')
                ->onDelete('cascade');
            $table->string('title')->nullable(true);
            $table->string('link')->nullable(true);
            $table->string('image_path');
            $table->unsignedSmallInteger('order')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banner_items');
        Schema::dropIfExists('banner_groups');
    }
}
