<?php

namespace Database\Seeders;

use App\Models\Jury\JuryGroup;
use App\Models\Jury\JuryMember;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class JurySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        JuryGroup::factory(10)->create(['year' => config('adwards.year')]);

        $groups = JuryGroup::all();

        foreach ($groups as $group) {
            JuryMember::factory($faker->numberBetween(3, 11))->create(['jury_group_id' => $group->id]);
        }
        // \App\Models\User::factory(10)->create();
    }
}
