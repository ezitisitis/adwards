<?php

namespace Database\Seeders;

use App\Models\Prize\Prize;
use App\Models\Prize\PrizeGroup;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class PrizesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        PrizeGroup::factory(2)->create(['year' => config('adwards.year')]);

        $prizes = PrizeGroup::all();

        foreach ($prizes as $prize) {
            Prize::factory($faker->numberBetween(3, 11))->create(['prize_group_id' => $prize->id]);
        }
    }
}
