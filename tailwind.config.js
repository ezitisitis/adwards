module.exports = {
    purge: [
        './resources/**/*.blade.php',
        './resources/**/*.js',
    ],
    darkMode: false, // or 'media' or 'class'
    theme: {
        extend: {
            lineHeight: {
                '2': '16px',
            },
            zIndex: {
                '9999': '9999'
            },
            height: {
                '40vh': '40vh'
            },
            width: {
                '70': '560px'
            },
            inset: {
                '0': '0',
                '2': '16px'
            },
        },
        spacing: {
            '0': '0',
            '1': '8px',
            '2': '16px',
            '3': '24px',
            '4': '32px',
            '5': '40px',
            '6': '48px',
            '7': '56px',
            '8': '64px',
            '9': '72px',
            '10': '80px',
            '11': '88px',
            '12': '96px',
            '13': '104px',
            '14': '112px',
            '15': '120px',
            '16': '128px',
            '17': '136px',
            '18': '144px',
            '19': '152px',
            '20': '160px',
            '21': '168px',
            '22': '176px',
            '23': '184px',
            '24': '192px',
            '25': '200px',
            '26': '208px',
            '27': '216px',
            '28': '224px',
            '29': '232px',
            '30': '240px',
            '31': '248px',
            '32': '256px',
            '71': '568px',
            '80': '640px',
        },
        container: {
            center: true,
            padding: '16px',
            width: {
                DEFAULT: '1168px'
            }
        },
        fontSize: {
            '1': ['8px', '16px'],
            '2': ['16px', '24px'],
            '3': ['24px', '32px'],
            '4': ['32px', '40px'],
            '5': ['40px', '48px'],
            '6': ['48px', '56px'],
            '7': ['56px', '64px'],
            '8': ['64px', '72px'],
            '9': ['72px', '80px'],
            '10': ['80px', '88px'],
        },
        screens: {
            'sm': '640px',
            'md': '880px',
            'lg': '1024px',
            'xl': '1280px',
            '2xl': '1536px',
        },
    },
    variants: {
        extend: {},
    },
    plugins: [
        function ({addComponents}) {
            addComponents({
                '.container': {
                    maxWidth: '100%',
                    '@screen lg': {
                        maxWidth: '1200px',
                    },
                }
            })
        },
        function ({addComponents}) {
            addComponents({
                '.container-small': {
                    maxWidth: '100%',
                    '@screen lg': {
                        maxWidth: '920px',
                    },
                }
            })
        },
        function ({addComponents}) {
            addComponents({
                '.menu-button': {
                    maxWidth: '100%',
                    '@screen lg': {
                        display: '920px',
                    },
                }
            })
        }
    ],
}
