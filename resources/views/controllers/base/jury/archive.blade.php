@extends('layout.main')

@push('css')
    <link href="{{ mix('assets/css/2021.css') }}" rel="stylesheet">
@endpush

@section('body')
    @include('components.base.navigation')
    {{-- Content goes here --}}
    @include('components.base.footer')
@endsection

@push('scripts')
    <script>
        let menuButton = document.getElementById('menuButton');
        let menu = document.getElementById('menu');
        menuButton.addEventListener('click', function (e) {
            menuButton.classList.toggle('is-active');
            menu.classList.toggle('invisible');
            menu.classList.toggle('opacity-100');
            setTimeout(200);
        });
    </script>
@endpush
