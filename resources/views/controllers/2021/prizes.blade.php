@extends('layout.main',
[
    'body_class' => 'prizes',
    'title' => trans('pages.prizes.title')
])

@push('css')
    <link href="{{ mix('assets/css/2021.css') }}" rel="stylesheet">
@endpush

@section('body')
    @include('components.2021.navigation')
    <div class="mt-5 md:mt-9 container mb-8 md:mb-16">
        @foreach($prize_groups as $prize_group)
            @include('elements.2021.page-title', [
                'heading' => $prize_group->title
            ])
            <div class="mx-auto w-full md:w-70 mb-16">
            @foreach($prize_group->prizes as $prize)
                <div class="prize-card mb-9 sm:grid sm:grid-cols-4 sm:gap-4">
                    <img class="mx-auto mb-2 w-20 sm:col-start-1" src="{{ Storage::url($prize->image_path) }}" alt="{{$prize->title}}">
                    <div class="sm:col-start-2 sm:col-end-5">
                        <h2 class="text-3 mb-1 uppercase">{{ $prize->title }}</h2>
                        <p class="text-2">{{ $prize->description }}</p>
                    </div>
                </div>
            @endforeach
            </div>
        @endforeach
    </div>
    @include('components.2021.footer')
@endsection

@push('scripts')
    <script>
        let menuButton = document.getElementById('menuButton');
        let menu = document.getElementById('menu');
        menuButton.addEventListener('click', function (e) {
            menuButton.classList.toggle('is-active');
            menu.classList.toggle('invisible');
            menu.classList.toggle('opacity-100');
            setTimeout(200);
        });
    </script>
@endpush
