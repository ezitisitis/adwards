@extends('layout.main')

@push('css')
    <link href="{{ mix('assets/css/2021.css') }}" rel="stylesheet">
@endpush

@section('body')
    @include('components.2021.navigation')
    @include('components.2021.hero')
    <div class="landing-content-block bg-contain bg-top bg-repeat-x flex flex-col">
{{--         style="background-image: url({{asset('assets/images/2021/sub-section.png')}})">--}}
        <div class="landing-content-heading">
            <h2 class="pt-10 md:pt-13 text-4 text-center uppercase mb-2">{{ $content->heading_first_line }}</h2>
            <h1 class="text-8 sm:text-10 text-center uppercase">{{ $content->heading_second_line }}</h1>
        </div>
        <div class="mt-6 mx-auto container-small px-2 grid gap-3 md:gap-6 grid-cols-2 landing-content-about">
            <div class="col-span-full md:col-start-1 md:col-end-2 mb-3 text-3 uppercase">
                {!! $content->top_description !!}
            </div>
            <div class="col-span-full md:col-start-2 md:col-end-3 text-2">
                {!! $content->detail_description !!}
            </div>
        </div>
        <a href="/"
           class="mx-auto mt-6 md:mt-12 mb-12 md:mb-16 py-2 px-4 mt-4 landing-content-button">{{ trans('buttons.submit_work') }}</a>
    </div>
    @include('components.2021.footer')
@endsection

@push('scripts')
    <script>
        let menuButton = document.getElementById('menuButton');
        let menu = document.getElementById('menu');
        menuButton.addEventListener('click', function (e) {
            menuButton.classList.toggle('is-active');
            menu.classList.toggle('invisible');
            menu.classList.toggle('opacity-100');
            setTimeout(200);
        });
    </script>
@endpush
