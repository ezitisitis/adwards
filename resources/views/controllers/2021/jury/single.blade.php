@extends('layout.main',
[
    'body_class' => 'jury-single',
    'title' => $content->name
])

@push('css')
    <link href="{{ mix('assets/css/2021.css') }}" rel="stylesheet">
@endpush

@section('body')
    @include('components.2021.navigation')
    <div class="mt-5 md:mt-9 container mb-8 md:mb-16">
        @include('elements.2021.page-title', [
            'heading' => $content->name,
            'sub_heading' => $content->position
        ])
        <div class="grid grid-cols-1 md:grid-cols-2 gap-12">
            @include('elements.2021.jury.avatar', [
                'path' => $content->image,
                'alt' => $content->name,
                'is_jury_president' => $content->president,
                'big_badge' => true
            ])
            <div class="element content text-2">
                @if($content->short_description)
                    <p class="text-4 mb-8">{{ $content->short_description }}</p>
                @endif
                {!! $content->description !!}
            </div>
        </div>
    </div>
    @include('components.2021.footer')
@endsection

@push('scripts')
    <script>
        let menuButton = document.getElementById('menuButton');
        let menu = document.getElementById('menu');
        menuButton.addEventListener('click', function (e) {
            menuButton.classList.toggle('is-active');
            menu.classList.toggle('invisible');
            menu.classList.toggle('opacity-100');
            setTimeout(200);
        });
    </script>
@endpush
