@extends('layout.main',
[
    'body_class' => 'archive',
    'title' => trans('jury.jury')
])

@push('css')
    <link href="{{ mix('assets/css/2021.css') }}" rel="stylesheet">
@endpush

@section('body')
    @include('components.2021.navigation')
    <div class="mt-5 md:mt-9 container">
        @include('elements.2021.page-title', ['heading' => trans('jury.jury_adwards')])
        @foreach($jury_groups as $jury_group)
            <div class="grid mb-20 md:mb-12 grid-cols-1 md:grid-cols-4 md:gap-x-6">
                @include('elements.2021.jury.group-title', ['title' => $jury_group->title])
                <div class="grid grid-cols-1 md:grid-cols-3 md:col-start-2 md:col-end-5 gap-y-7 md:gap-y-5 gap-x-11">
                    @foreach($jury_group->members as $member)
                        @include('components.2021.jury-card', ['member' => $member])
                    @endforeach
                </div>
            </div>
        @endforeach
    </div>
    @include('components.2021.footer')
@endsection

@push('scripts')
    <script>
        let menuButton = document.getElementById('menuButton');
        let menu = document.getElementById('menu');
        menuButton.addEventListener('click', function (e) {
            menuButton.classList.toggle('is-active');
            menu.classList.toggle('invisible');
            menu.classList.toggle('opacity-100');
            setTimeout(200);
        });
    </script>
@endpush
