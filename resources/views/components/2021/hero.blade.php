<div class="mt-5 md:mt-9 h-40vh md:h-71 bg-cover bg-no-repeat bg-center xl:bg-contain hero"
     style="background-image: url('/storage/{{ $content->hero }}')">
</div>
@if($content->hero_link && $content->hero_text)
    <div class="flex md:hidden hero-subsection">
        <a href="{{ $content->hero_link }}"
           class="hero-button text-white py-2 px-4 m-auto mt-4">{{ $content->hero_text }}</a>
    </div>
@endif
