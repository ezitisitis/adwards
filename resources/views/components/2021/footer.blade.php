<footer class="footer pt-12 pb-2">
    <div class="container flex flex-row flex-wrap">
        @foreach($banners as $bannerGroup)
            @if(!$loop->first)
                <?php $multipleRows = true ?>
            @endif
            <div class="banner mx-auto mb-5 md:mb-16 @if($loop->first) w-full @endif">
                <span class="mb-5 w-full text-center block banner-heading">{{ $bannerGroup->title }}</span>
                <div class="flex flex-row flex-wrap">
                    @foreach($bannerGroup->banners as $bannerImage)
                        @if(isset($multipleRows))
                            <img class="mb-7 m-auto @if(!$loop->first) ml-12 @endif" src="/storage/{{ $bannerImage->image_path }}" alt="{{ $bannerImage->title }}">
                        @else
                            <img class="mb-7 m-auto" src="/storage/{{ $bannerImage->image_path }}" alt="{{ $bannerImage->title }}">
                        @endif
                    @endforeach
                </div>
            </div>
        @endforeach
    </div>
</footer>
