<div class="jury-card @if(isset($grid)) {{$grid}} @endif">
    @include('elements.2021.jury.avatar', [
        'path' => $member->image,
        'is_jury_president' => $member->president,
        'alt' => $member->name
    ])
    <h3 class="text-3 mb-1 ">{{ $member->name }}</h3>
    <h4 class="text-2 uppercase mb-1">{{ $member->position }}</h4>
    <h5 class="text-2 mb-2 md:mb-3">{{ $member->company }} ({{ $member->city }})</h5>
    @include('elements.2021.read-more', [
        'icon' => 'arrow-right',
        'slug' => '/zhurija/' . $member->slug,
        'modificator_class' => ''
    ])
</div>
