<meta property="og:title" content="{{ config('app.name', 'Laravel') }}- {{ $title ?? '' }}"/>
<meta property="og:type" content="{{ $open_graph_type ?? 'website' }}"/>
<meta property="og:url" content="{{url()->current()}}"/>
<meta property="og:image" content=""/>
