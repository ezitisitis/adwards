<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('components.open-graph')
    <title>{{ isset($title) ? config('app.name', 'Laravel') . '- ' . $title : config('app.name', 'Laravel') }}</title>

    <link href="{{ mix('assets/css/tailwind.css') }}" rel="stylesheet">
    @stack('css')

    @if(config('services.google.analytics.id'))
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id={{ config('services.google.analytics.id') }}"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', '{{ config('services.google.analytics.id') }}');
        </script>
    @endif
</head>
<body @if(isset($body_class)) class="{{$body_class}}" @endif>
@yield('body')
<script src="{{ mix('assets/js/app.js') }}" defer></script>
@stack('scripts')
</body>
</html>
