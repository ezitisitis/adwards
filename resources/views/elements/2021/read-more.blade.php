<a href="{{$slug}}" class="read-more text-2 @if(isset($icon))flex flex-row @endif {{ $modificator_class ?? '' }}">
    @if(!isset($icon))
        {{ trans('buttons.read_more') }}
    @else
        <span>
            {{ trans('buttons.read_more') }}
        </span>
        @include('elements.2021.icon', [
            'icon' => $icon,
            'width' => '15',
            'height' => '15',
            'class' => isset($icon_modificator_class) ? 'ml-2 my-auto ' . $icon_modificator_class : 'ml-2 my-auto',
        ])
    @endif
</a>
