<svg class="icon {{ $class ?? '' }}" width="{{ $width }}" height="{{ $height }}">
    <use xlink:href="/assets/svg/2021.svg#{{ $icon }}"></use>
</svg>
