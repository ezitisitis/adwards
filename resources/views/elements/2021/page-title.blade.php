<div class="mt-7 md:mt-16 mb-6 md:mb-12 text-center">
    <h1 class="element page-title text-6 md:text-9 uppercase">{{$heading}}</h1>
    @if(isset($sub_heading))
        <p class="element page-sub-title text-3 w-full md:mt-3">{{$sub_heading}}</p>
    @endif
</div>

