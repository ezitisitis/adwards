<div class="element avatar relative">
    <img class="avatar-image mb-2 md:mb-0 {{ $modificator_class ?? '' }}"
         src="{{ Storage::url($path) }}"
         @if($alt) alt="{{ $alt }} @endif">
    @if($is_jury_president)
        <img class="absolute bottom-2 right-2 @if(isset($big_badge) && $big_badge) w-16 h-16 @else w-9 h-9 @endif"
             src="{{ asset('assets/images/2021/president.png') }}">
    @endif
</div>
