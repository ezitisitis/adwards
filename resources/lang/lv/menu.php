<?php

return [
    'jury' => 'Žūrija',
    'awards' => 'Apbalvojumi',
    'categories' => 'Kategorijas',
    'about_adwards' => 'Par Adwards',
    'rules' => 'Noteikumi'
];
