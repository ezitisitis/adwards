<?php

return [
    'jury' => 'Jury',
    'awards' => 'Awards',
    'categories' => 'Categories',
    'about_adwards' => 'About Adwards',
    'rules' => 'Rules',
    'submit' => 'Submit a job'
];
