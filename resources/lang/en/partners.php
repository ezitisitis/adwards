<?php

return [
    'in_collaboration_with' => 'In collaboration with',
    'media_partners' => 'Media partners',
    'papers_and_materials' => 'Papers and materials'
];
