<?php

return [
    'fields' => [
        'hero' => 'Hero image',
        'heading' => 'Heading',
        'slogan' => 'Slogan',
        'description' => 'Description',
        'year' => 'Year',
        'title' => 'Title',
        'order' => 'Order',
        'members' => 'Members',
        'banners' => 'Banners',
        'name' => 'Name',
        'slug' => 'Slug',
        'position' => 'Position',
        'company' => 'Company',
        'city' => 'City',
        'short_description' => 'Short description',
    ],
];
