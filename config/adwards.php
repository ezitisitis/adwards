<?php

return [
    'year' => env('ADWARDS_YEAR', date('Y')),

    'submit_work_active' => env('ADWARDS_SUBMIT_WORK_ACTIVE', 'false'),
];
