<?php

namespace App\Models\Banner;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BannerItem extends Model
{
    use HasFactory;

    protected $table = 'banner_items';
}
