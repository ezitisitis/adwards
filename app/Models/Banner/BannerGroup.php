<?php

namespace App\Models\Banner;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BannerGroup extends Model
{
    use HasFactory;

    protected $table = 'banner_groups';

    public function banners()
    {
        return $this->hasMany(BannerItem::class, 'banner_group_id')->orderBy('order');
    }

    public static function get(int $year)
    {
         $collection = self::where('year', $year)
            ->get();

         return $collection;
    }
}
