<?php

namespace App\Models\Prize;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PrizeGroup extends Model
{
    use HasFactory;

    protected $table = 'prize_groups';

    protected $fillable = [
        'year',
        'title',
    ];

    public function prizes()
    {
        return $this->hasMany(Prize::class, 'prize_group_id');
    }
}
