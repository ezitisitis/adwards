<?php

namespace App\Models\Jury;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JuryGroup extends Model
{
    use HasFactory;

    protected $fillable = [
        'year',
        'title',
        'order',
    ];

    protected $table = 'jury_groups';

    public function members()
    {
        return $this->hasMany(JuryMember::class, 'jury_group_id')->orderBy('order');
    }
}
