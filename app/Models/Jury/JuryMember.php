<?php

namespace App\Models\Jury;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JuryMember extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'slug',
        'president',
        'position',
        'company',
        'city',
        'short_description',
        'description',
    ];

    protected $table = 'jury_members';
}
