<?php

namespace App\Http\Controllers\Base;

use App\Models\LandingPage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LandingPageController extends Controller
{
    /**
     * @param int|null $year
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(?int $year = null)
    {
        $year = !is_null($year) ? $year : config('adwards.year');

        return view('controllers.' . $year . '.landing',  [
            'content' => LandingPage::where('year', $year)->first()
        ]);
    }
}
