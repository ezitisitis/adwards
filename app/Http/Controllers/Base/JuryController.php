<?php

namespace App\Http\Controllers\Base;

use App\Models\Jury\JuryGroup;
use App\Models\Jury\JuryMember;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class JuryController extends Controller
{
    public function archive(?int $year = null)
    {
        $year = !is_null($year) ? $year : config('adwards.year');

        return view('controllers.' . $year . '.jury.archive',  [
            'jury_groups' => JuryGroup::where('year', $year)->orderBy('order')->get()
        ]);
    }

    public function single(string $slug, ?int $year = null)
    {
        $year = !is_null($year) ? $year : config('adwards.year');

        return view('controllers.' . $year . '.jury.single', [
            'content' => JuryMember::where('slug', $slug)->first()
        ]);
    }
}
