<?php

namespace App\Http\Controllers\Base;

use App\Http\Controllers\Controller;
use App\Models\Prize\PrizeGroup;

class PrizesPageController extends Controller
{
    public function index(?int $year = null)
    {
        $year = !is_null($year) ? $year : config('adwards.year');

        return view('controllers.' . $year . '.prizes',  [
            'prize_groups' => PrizeGroup::where('year', $year)->orderBy('order')->get()
        ]);
    }
}
