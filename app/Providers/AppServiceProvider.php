<?php

namespace App\Providers;

use App\Models\Banner\BannerGroup;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $banners = BannerGroup::get(config('adwards.year'));

        if (!is_null($banners)) {
            view()->share('banners', $banners);
        }
    }
}
