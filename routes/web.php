<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/zhurija', 'App\Http\Controllers\TwentyOne\JuryController@archive');
Route::get('/zhurija/{slug}', 'App\Http\Controllers\TwentyOne\JuryController@single');
Route::get('/', 'App\Http\Controllers\TwentyOne\LandingPageController@index')->name('landing.current');
Route::get('/{year}', 'App\Http\Controllers\Base\LandingPageController@index')->where('year', '[0-9]+')->name('landing.year');
Route::get('/apbalvojumi', 'App\Http\Controllers\TwentyOne\PrizesPageController@index')->name('prizes');
