const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/assets/js')
    .sass('resources/scss/2021.scss', 'public/assets/css/2021.css')
    .postCss('resources/css/tailwind.css', 'public/assets/css', [
        require("tailwindcss"),
    ])
    .copyDirectory('resources/images', 'public/assets/images')
    .copyDirectory('resources/svg', 'public/assets/svg')
    .copyDirectory('resources/fonts', 'public/assets/fonts');

if (mix.inProduction()) {
    mix.version();
}
